import { DataObjectState } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
 async function beforeSaveAsync(event) {

}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    //  get context
     const context = event.model.context;
    if (event.state === DataObjectState.Insert) {
        //  create application user
        const candidateGroup = await context.model('Group').where('name').equal('Candidates').silent().getItem();
        if (!candidateGroup) {
            throw new Error('Candidates group is missing');
        }
        // TODO: Change this when user model becomes updatable
        const applicationUser = {
            userId: event.target.id,
            applicationId: 'GRWEB',
            roleId: candidateGroup.id
        };
        // // save applicationUser
        await context.model('ApplicationUser').silent().save(applicationUser);
        // add user to groupMembers

        event.target.groups = [candidateGroup.id];
        await context.model('CandidateUser').silent().save( event.target);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
