import { DataObjectState, DataConfigurationStrategy } from '@themost/data';
import { ApplicationService, TraceUtils, DataError } from '@themost/common';
import path from 'path';
/**
 * 
 * @param {DataEventArgs} event 
 */
async function afterSaveAsync(event) {

    const context = event.model.context;
    let shouldContinue = false;
    // get candidate student user
    const user = await context.model(event.model.name)
        .where('id').equal(event.target.id)
        .select('user').silent().value();
    if (event.state === DataObjectState.Insert) {
        shouldContinue = false;
    } else if (event.state === DataObjectState.Update) {
        const previous = event.previous;
        if (previous == null) {
            throw new DataError('E_STATE', 'Invalid configuration. The previous state of an object cannot be determined.', null, 'CandidateStudent');
        }
        shouldContinue = (previous.user == null) && (user != null);
    }
    if (shouldContinue === false) {
        // exit
        return;
    }
    const service = context.getApplication().getService(function OAuth2ClientService() {});
    if (service == null) {
        throw new Error('Invalid application configuration. Authentication client service is missing or is inaccessible state.');
    }
    if (service.settings.adminAccount == null) {
        throw new Error('Invalid application configuration. Authentication client service is not properly configured to handle user operations.');
    }
    // create new action
    await context.model('CreateCandidateUserAction').save({
        object: user,
        actionStatus: {
            alternateName: 'ActiveActionStatus'
        }
    });
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
 function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

class AfterCreateCandidateUser extends ApplicationService {
    constructor(app) {
        super(app);
        this.install('CandidateUser', __filename);
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get StudentRequestAction model definition
        const model = configuration.getModelDefinition('CandidateStudent');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update StudentRequestAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info(`Services: ${this.constructor.name} service has been successfully installed`);
        }
    }
}

export {
    afterSave,
    AfterCreateCandidateUser
}