// import { DataError } from '@themost/common';
import { DataModel } from '@themost/data';

/*
async function beforeSaveAsync(event) {
	const context = event.model.context;
	const target = event.target;
	const previous = event.previous;
	const inscriptionMode =
		target.inscriptionMode || (previous && previous.inscriptionMode);
	const inscriptionYear =
		target.inscriptionYear || (previous && previous.inscriptionYear);
	const inscriptionNumber =
		target.inscriptionNumber || (previous && previous.inscriptionNumber);
	if (!(inscriptionYear && inscriptionMode && inscriptionNumber)) {
		throw Object.assign(
			new DataError(
				'E_CONSTRAINT_ATTRIBUTES',
				'The inscription year, inscription number and inscription mode may not be empty because they are part of a unique constraint.',
				null,
				'CandidateStudent'
			),
			{
				statusCode: 409,
			}
		);
	}
	// try to find if another candidate exists with that attributes tuplet
	const candidate = await context
		.model('CandidateStudent')
		.where('inscriptionYear')
		.equal(inscriptionYear.id || inscriptionYear)
		.and('inscriptionMode')
		.equal(inscriptionMode.id || inscriptionMode)
		.and('inscriptionNumber')
		.equal(inscriptionNumber.toString())
		.and('id')
		.notEqual(target.id) // notEqual null for insert state
		.select('id')
		.silent()
		.getItem();
	// if it does
	if (candidate) {
		// set target id to candidate's id
		event.target.id = candidate.id;
		// set previous state if not set
		if (event.previous == null) {
			event.previous = event.target;
		}
		// send for update on the fly
		event.state = 2;
	}
}
*/
/*
export function beforeSave(event, callback) {
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
*/

async function beforeRemoveAsync(event) {
  const model = event.model;
  const getReferenceMappings = DataModel.prototype.getReferenceMappings;
  model.getReferenceMappings = async function () {
    const res = await getReferenceMappings.bind(this)();
    // remove CandidateStudentUploadActionResult mappings
    const mappings = ['CandidateStudentUploadActionResult'];
    return res.filter((mapping) => {
      return mappings.indexOf(mapping.childModel) < 0;
    });
  };
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
  return beforeRemoveAsync(event)
    .then(() => {
      return callback();
    })
    .catch((err) => {
      return callback(err);
    });
}
