import { DataObjectState } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    // get original data
    const actionStatus = await context.model('StudyProgramRegisterAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    let shouldUpdateChildren = false;
    const ValidActionStatusTypes = [
        'CompletedActionStatus',
        'PotentialActionStatus',
        'ActiveActionStatus',
        'FailedActionStatus',
        'CancelledActionStatus'
    ];
    if (event.state === DataObjectState.Insert) {
        if (ValidActionStatusTypes.includes(actionStatus)) {
            shouldUpdateChildren = true;
        }
    }
    // get previous status
    let previousStatus = 'UnknownActionStatus';
    if (event.state === DataObjectState.Update) {
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        }
        if (ValidActionStatusTypes.includes(actionStatus) && actionStatus !== previousStatus) {
            shouldUpdateChildren = true;
        }
    }
    if (shouldUpdateChildren) {
        const children = await context.model('CourseClassRegisterAction')
            .where('initiator').equal(event.target.id)
            .select('id', 'actionStatus').silent().getItems();
        // when
        if (children.length) {
            children.forEach((child) => {
                if (actionStatus === 'CompletedActionStatus') {
                    if (child.actionStatus.alternateName === 'ActiveActionStatus') {
                        child.actionStatus = {
                            alternateName: 'CompletedActionStatus'
                        }
                    }
                }
                // when an application is becoming children should also become active
                if (previousStatus === 'PotentialActionStatus' && actionStatus === 'ActiveActionStatus') {
                    child.actionStatus = {
                        alternateName: 'ActiveActionStatus'
                    };
                }
                // when an application is reverted to potential children should be updated alos
                if (previousStatus === 'ActiveActionStatus' && actionStatus === 'PotentialActionStatus') {
                    child.actionStatus = {
                        alternateName: 'PotentialActionStatus'
                    };
                }
                // when an application is being rejected children should also be rejected
                if (actionStatus === 'CancelledActionStatus' && previousStatus !== 'CancelledActionStatus') {
                    child.actionStatus = {
                        alternateName: 'CancelledActionStatus'
                    };
                }
                // when an application is being activated children should also be activated
                if (actionStatus === 'ActiveActionStatus' && previousStatus !== 'ActiveActionStatus') {
                    child.actionStatus = {
                        alternateName: 'ActiveActionStatus'
                    };
                }
            });
        }
        await context.model('CourseClassRegisterAction').silent().save(children);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}