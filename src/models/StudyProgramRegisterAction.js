import { EdmMapping, EdmType } from "@themost/data";
import EnableAttachmentModel from './EnableAttachmentModel';
import { TraceUtils } from '@themost/common';
function tryCloseStream(stream) {
    if (stream && typeof stream.close === 'function') {
        try {
            stream.close();
        } catch (error) {
            TraceUtils.warn(`(tryCloseStream) An error occurred while trying to close a stream.`);
            TraceUtils.warn(error);
        }
    }
}

class StudyProgramRegisterAction extends EnableAttachmentModel {
    constructor() {
        super();
    }
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('AddAttachment', 'Attachment')
    async addAttachment(file, extraAttributes) {
        try {
            const attachment = Object.assign({
                name: file.contentFileName,
                originalname:file.contentFileName
            }, file, extraAttributes);
            const result = await super.addAttachment(attachment);
            tryCloseStream(file);
            return result;
        } catch(err) {
            // try to close stream
            tryCloseStream(file);
            // and throw error
            throw err;
        }
        
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RemoveAttachment', 'Attachment')
    async removeAttachment(attachment) {
        return await super.removeAttachment(attachment.id);
    }

    /**
     * Set preferred course classes
     * @param {Array<any>} items
     */
    @EdmMapping.param('items', EdmType.CollectionOf('CourseClassRegisterAction'), true, true)
    @EdmMapping.action('CourseRegistrations', EdmType.CollectionOf('CourseClassRegisterAction'))
    async setCourseRegistrations(items) {
        // set initiator
        items.forEach((item) => {
            item.initiator = {
                id: this.getId()
            };
        });
        return this.context.model('CourseClassRegisterAction').save(items);
    }

    /**
     * Set preferred internships
     * @param {Array<any>} items
     */
    @EdmMapping.param('items', EdmType.CollectionOf('InternshipRegisterAction'), true, true)
    @EdmMapping.action('InternshipRegistrations', EdmType.CollectionOf('InternshipRegisterAction'))
    async setInternshipRegistrations(items) {
        // set initiator
        items.forEach((item) => {
            item.initiator = {
                id: this.getId()
            };
        });
        return this.context.model('InternshipRegisterAction').save(items);
    }

    /**
     * Add action messages
     * @param {Array<any>} items
     */
    @EdmMapping.param('item', 'StudyProgramRegisterActionMessage', true, true)
    @EdmMapping.action('messages', 'StudyProgramRegisterActionMessage')
    async postMessages(item) {
        // set initiator
        item.action = this.getId();
        return this.context.model('StudyProgramRegisterActionMessage').save(item);
    }

    /**
     * Gets item review
     */
    @EdmMapping.func('review', 'StudyProgramRegisterActionReview')
    getReview() {
        return this.context.model('StudyProgramRegisterActionReview')
            .where('itemReviewed').equal(this.getId()).prepare();
    }

    /**
     * Set item review
     * @param {*} item
     */
    @EdmMapping.param('item', 'StudyProgramRegisterActionReview', true, true)
    @EdmMapping.action('review', 'StudyProgramRegisterActionReview')
    async setReview(item) {
        const StudyProgramRegisterActionReviews = this.context.model('StudyProgramRegisterActionReview');
        // infer object state
        const currentReview = await StudyProgramRegisterActionReviews.where('itemReviewed').equal(this.getId()).getItem();
        if (currentReview == null) {
            if (item == null) {
                return;
            }
            // a new item is going to be inserted
            delete item.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        } else {
            if (item == null) {
                // delete review
                StudyProgramRegisterActionReviews.remove(currentReview);
            }
            // up
            item.id = currentReview.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        }
        return StudyProgramRegisterActionReviews.save(item);
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('message', 'Object', true, true)
    @EdmMapping.param('attachment', EdmType.EdmStream, false)
    @EdmMapping.action('SendMessage', 'RegisterActionMessage')
    sendMessage(attachment, message) {
        const thisArg = this;
        return new Promise((resolve, reject) => {
            this.context.db.executeInTransaction((cb) => {
                (async () => {
                    const StudyProgramRegisterActionMessage = thisArg.context.model('StudyProgramRegisterActionMessage');
                    const newMessage = StudyProgramRegisterActionMessage.convert(message);
                    Object.assign(newMessage, {
                        action: thisArg.getId()
                    });
                    // add message
                    await StudyProgramRegisterActionMessage.save(newMessage);
                    // if message has an attachment
                    if (attachment) {
                        // prepare attachment data
                        const newAttachment = Object.assign({
                            name: attachment.contentFileName,
                            originalname: attachment.contentFileName
                        }, attachment);
                        // and add attachment
                        await newMessage.addAttachment(newAttachment)
                    }
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(message);
            })
        });
    }


}

module.exports = StudyProgramRegisterAction;
