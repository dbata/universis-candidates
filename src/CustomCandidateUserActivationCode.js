import {RandomCandidateUserActivationCode} from './RandomCandidateUserActivationCode';
class CustomCandidateUserActivationCode extends RandomCandidateUserActivationCode {
    constructor(app) {
        super(app);
    }
    /**
     * 
     * @param {DataContext} context 
     * @param {*} candidate 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    generate(context, candidate) {
        const superGenerate = super.generate;
        return context.model('CandidateStudent').where('id').equal(candidate)
            .select('customField1').silent().value().then((value) => {
                if (value && value.length) {
                    return value;
                }
                return superGenerate.bind(this)(context, candidate);
            });
    }

}

export {
    CustomCandidateUserActivationCode
};
