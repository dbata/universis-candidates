import {ApplicationService} from '@themost/common';
import {ODataModelBuilder} from "@themost/data";
import {DataConfigurationStrategy} from '@themost/data';
import fs from 'fs';
import path from 'path';
import pluralize from 'pluralize';
import './DataFilterExtensions';
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { StudyProgramReplacer } from './StudyProgramReplacer';

export class CandidateService extends ApplicationService {
    /**
     * @param {IApplication|*} app
     */
    constructor(app) {
        super(app);
        // extend StudyProgram
        new StudyProgramReplacer(app).apply();
        this.install();
        // extend application service router
        if (app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }
        // register templates
        // get MailTemplateService by name
        const mailTemplateService = this.getApplication().getService(function MailTemplateService() { });
        if (mailTemplateService != null) {
            mailTemplateService.extraTemplates.push({
                name: 'new-candidate-user',
                path: path.resolve(__dirname, './templates/new-candidate-user/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'new-message',
                path: path.resolve(__dirname, './templates/new-message/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'send-sms',
                path: path.resolve(__dirname, './templates/send-sms/html.ejs')
            });
            // mailTemplateService.extraTemplates.push({
            //     name: 'direct-message',
            //     path: path.resolve(__dirname, './templates/direct-message/html.ejs')
            // });
            mailTemplateService.extraTemplates.push({
                name: 'register-status-change',
                path: path.resolve(__dirname, './templates/register-status-change/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'direct-email-to-candidate',
                path: path.resolve(__dirname, './templates/direct-email-to-candidate/html.ejs')
            });
        }

    }

    install() {
        // place your code here
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // noinspection JSValidateTypes
        /**
         * get model builder
         * @type {ODataModelBuilder}
         */
        const builder = this.getApplication().getStrategy(ODataModelBuilder);
             // load models
        const files = fs.readdirSync(path.resolve(__dirname, 'config/models')).filter(file => path.extname(file) === '.json');
        files.forEach( file => {
            // load model definition
            const model = require(path.resolve(__dirname, 'config/models', file));
            // try to get model from application (if model definition does not exists in parent configuration)
            if (configuration.model(model.name) == null) {
                // set model definition
                if (Array.isArray(model.eventListeners) ) {
                    model.eventListeners.forEach(eventListener => {
                        if (eventListener.type.indexOf('.') === 0) {
                            eventListener.type = path.resolve(__dirname,  eventListener.type);
                        }
                    });
                }
                if (model.classPath && model.classPath.indexOf('.')===0) {
                    model.classPath= path.resolve(__dirname, model.classPath);

                }
                configuration.setModelDefinition(model);
            }
        });

        files.forEach( file => {
            // load model definition
            const model = require(path.resolve(__dirname, 'config/models', file));
            if (builder) {
                let entitySet =  pluralize.plural(model.name);
                // add entity set (for odata endpoints)
                builder.addEntitySet(model.name, entitySet);
                // check if model is hidden
                if (model.hidden) {
                    // and ignore it
                    builder.ignore(model.name);
                }
                // refresh model entity set
                (function refreshEntitySet(entityType, entitySet) {
                    builder.removeEntitySet(entitySet);
                    builder.addEntitySet(entityType, entitySet);
                })(model.name,entitySet);
            }
        });
    }

    // noinspection JSUnusedGlobalSymbols
    uninstall() {
        // place your code here
    }

}
